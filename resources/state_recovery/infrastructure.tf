provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "marphio"
}

terraform {
  backend "s3" {
    bucket  = "terraform-remote-state-marphio"
    key     = "demo/terraform.tfstate"
    region  = "eu-west-1"
    profile = "marphio"
  }
}

data "terraform_remote_state" "base-infrastructure" {
  backend = "s3"

  config {
    bucket  = "terraform-remote-state-marphio"
    key     = "base-infrastructure/terraform.tfstate"
    region  = "eu-west-1"
    profile = "marphio"
  }
}

resource "aws_key_pair" "demo-public" {
  key_name   = "demo-public"
  public_key = ""
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow all inbound ssh traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ebs_volume" "primary" {
  count             = "${var.instance_count}"
  availability_zone = "${var.availability_zones[count.index % data.terraform_remote_state.base-infrastructure.subnets_total]}"
  size              = "${var.disk_size}"
  type              = "gp2"
  snapshot_id       = "${var.snapshot_id_primary}"

  tags {
    Name = "${var.cluster_name}-${count.index}-primary"
  }
}

resource "aws_instance" "my-cool-vm" {
  count                       = "${var.instance_count}"
  subnet_id                   = "${lookup(data.terraform_remote_state.base-infrastructure.subnets, count.index % data.terraform_remote_state.base-infrastructure.subnets_total)}"
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${aws_key_pair.demo-public.key_name}"
  associate_public_ip_address = true

  vpc_security_group_ids = [
    "${aws_security_group.allow_ssh.id}",
  ]

  depends_on = [
    "aws_security_group.allow_ssh",
  ]

  tags {
    Name = "${var.cluster_name}-${count.index}"
  }
}

resource "aws_volume_attachment" "primary-attachment" {
  count       = "${var.instance_count}"
  device_name = "/dev/xvdb"
  volume_id   = "${element(aws_ebs_volume.primary.*.id, count.index)}"
  instance_id = "${element(aws_instance.my-cool-vm.*.id, count.index)}"

  depends_on = [
    "aws_ebs_volume.primary",
    "aws_instance.my-cool-vm",
  ]
}
