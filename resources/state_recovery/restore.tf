variable "snapshot_id_primary" {
  default     = ""
  description = "This value is used to define the snapshot id of the disk that is attached to your machine"
}
