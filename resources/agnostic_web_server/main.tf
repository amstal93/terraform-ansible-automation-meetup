locals {
  public_key_path = "${var.private_key_path}.pub"
  key_name        = "${basename(var.private_key_path)}"
}

# AWS
module "aws_site_vm" {
  source          = "./modules/aws/vm"
  public_key_path = "${local.public_key_path}"
  key_name        = "${local.key_name}"
}

module "aws_lb_vm" {
  source          = "./modules/aws/vm"
  public_key_path = "${local.public_key_path}"
  key_name        = "${local.key_name}"
}

#Azure
module "azure_site_vm" {
  source          = "./modules/azure/vm"
  public_key_path = "${local.public_key_path}"
  key_name        = "${local.key_name}"
}

# Ansible
locals {
  machine_ips    = ["${module.azure_site_vm.public_ip}", "${module.aws_site_vm.public_ip}"]
  cloud_provider = ["azure", "aws"]
}

resource "null_resource" "web_ansible" { 
  count = 2 #"${length(local.machine_ips)}"

  triggers {
    key = "${uuid()}"
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu --private-key ${var.private_key_path} -e 'ansible_python_interpreter=/usr/bin/python3' -i '${local.machine_ips[count.index]},' --extra-vars 'cloud_provider=${local.cloud_provider[count.index]}' ./ansible/playbooks/webserver.yml"
  }
}

resource "null_resource" "lb_ansible" {
  depends_on = ["null_resource.web_ansible"]
  
  triggers {
    key = "${uuid()}"
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ubuntu --private-key ${var.private_key_path} -e 'ansible_python_interpreter=/usr/bin/python3' -i '${module.aws_lb_vm.public_ip},' --extra-vars '{'servers': ['${join("', '", local.machine_ips)}']}' ./ansible/playbooks/loadbalancer.yml"
  }
}
