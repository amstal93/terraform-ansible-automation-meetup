provider "aws" {
  region                  = "eu-west-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "marphio"
}

terraform {
  backend "s3" {
    bucket  = "terraform-remote-state-marphio"
    key     = "base-infrastructure/terraform.tfstate"
    region  = "eu-west-1"
    profile = "marphio"
  }
}

resource "aws_subnet" "subnet-1a" {
  vpc_id                  = "${var.vpc_id}"
  cidr_block              = "172.31.16.0/20"
  availability_zone       = "eu-west-1a"
  map_public_ip_on_launch = true

  tags {
    Name = "subnet-1a"
  }
}

resource "aws_subnet" "subnet-1b" {
  vpc_id                  = "${var.vpc_id}"
  cidr_block              = "172.31.32.0/20"
  availability_zone       = "eu-west-1b"
  map_public_ip_on_launch = true

  tags {
    Name = "subnet-1b"
  }
}

resource "aws_subnet" "subnet-1c" {
  vpc_id                  = "${var.vpc_id}"
  cidr_block              = "172.31.0.0/20"
  availability_zone       = "eu-west-1c"
  map_public_ip_on_launch = true

  tags {
    Name = "subnet-1c"
  }
}
